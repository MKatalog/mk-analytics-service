import os
from app.config import get_config

def run_codegen():
    config = get_config()
    os.system(f"sqlalchemy-codegen {config.psql.psql_conn} --models_layer --outdir app/db")