import random

from sqlalchemy.orm import Session
from sqlalchemy.sql import func
from uuid import uuid4
from datetime import datetime

from app.db.cn import get_cn_db
from app.db.psql import get_psql_db
from app.db.models import Products, ProductLinks
from app.config import get_config
from app.recomend.models import Metrics, ActionType
from app.analytic.models import HistoryStatistic, StatisticTypeEnum

def generate_fake_metric():
    config = get_config()
    cn_db: Session = get_cn_db().__next__()
    psql_db: Session = get_psql_db().__next__()
    
    fake_users_ids = [
        'c38f2d55-47e2-494a-a185-69225d727f5f',
        'e985f6b7-e4a5-4bc7-a1c0-114e4e1997fb',
        '71522960-49c0-465b-af81-dce50db1b9dc',
        'd196cd96-65ff-4c12-a1c9-5c792cd919ad',
        '09396a09-6e45-42eb-8481-9366014efa19',
        '8ecdd0bb-af20-48ce-8115-46cabe9deea5',
        'a2330fe7-2688-4dc5-a9b5-43d88e53135f',
        '7ccb5149-89ae-4601-b07a-8070bb57ccc4',
        '8eec4028-92b0-4907-a7af-cac473e8ac65',
        'a294554e-e134-4063-bcaf-760d882fbd3f',
    ]

    products = psql_db.query(Products.Product).order_by(func.random()).limit(100).all()

    fake_data: list[Metrics] = []

    for _ in range(config.cn.cn_fake_count):
        random_product = random.choice(products)
        fake_data.append(Metrics(
            id=str(uuid4()), 
            item_id=str(random_product.Id),
            user_id=random.choice(fake_users_ids), 
            category_id=str(random_product.CategoryId),
            action_type=random.choice([ActionType.PRODUCT_CLICK, ActionType.PRODUCT_LIKE]),
            create_date=datetime.fromisoformat(f'2024-03-{random.randrange(10, 24)}T{random.randrange(10, 23)}:{random.randrange(10, 59)}:{random.randrange(10, 59)}.795Z')
        ))
    
    cn_db.add_all(fake_data)
    cn_db.commit()

def _find_links(item: ProductLinks.ProductLink, id: str) -> bool:
    if item.ProductId == id:
        return True
    else:
        return False

def _rand_number(value: float) -> float:
    if value == 0.0:
        return round(random.uniform(200.0, 300.0), 1)
    else:
        return round(random.uniform(value * 0.9, value * 1.1), 1)

def generate_fake_analytic():
    cn_db: Session = get_cn_db().__next__()
    psql_db: Session = get_psql_db().__next__()

    products = psql_db.query(Products.Product).all()
    product_links = psql_db.query(ProductLinks.ProductLink).all()

    items_to_add: list[HistoryStatistic] = []

    current_month = datetime.now().month
    current_year = datetime.now().year

    for product in products:
        product_links_by_product = list(filter(lambda x: _find_links(x, product.Id), product_links))
        above_month_sub = 0
        for i in range(12):
            day = 0
            month = current_month
            year = current_year

            if i % 2 == 0:
                day = 14
            else:
                day = 28
            
            if current_month - int((i + 2) / 2) < 1:
                month = 12 - above_month_sub
                if i % 2 != 0:
                    above_month_sub += 1
            else:
                month = current_month - int((i + 2) / 2)
            
            if month > current_month:
                year = year - 1

            create_date = datetime(day=day, month=month, year=year)

            for stat_type in [StatisticTypeEnum.PRICE, StatisticTypeEnum.QUANTITY, StatisticTypeEnum.DELIVERY_TIME]:
                if stat_type == StatisticTypeEnum.PRICE:
                    prices = list(map(lambda x: x.Price, product_links_by_product))

                    items_to_add.append(HistoryStatistic(
                        id=str(uuid4()),
                        product_id=str(product.Id),
                        stat_type=stat_type,
                        x=create_date.isoformat(),
                        y=str(_rand_number(sum(prices) / len(prices))),
                        create_date=create_date
                    ))
                if stat_type == StatisticTypeEnum.QUANTITY:
                    quantities = list(map(lambda x: x.Quantity, product_links_by_product))

                    items_to_add.append(HistoryStatistic(
                        id=str(uuid4()),
                        product_id=str(product.Id),
                        stat_type=stat_type,
                        x=create_date.isoformat(),
                        y=str(_rand_number(sum(quantities) / len(quantities))),
                        create_date=create_date
                    ))
                if stat_type == StatisticTypeEnum.DELIVERY_TIME:
                    items_to_add.append(HistoryStatistic(
                        id=str(uuid4()),
                        product_id=str(product.Id),
                        stat_type=stat_type,
                        x=create_date.isoformat(),
                        y=str(random.randint(3, 12)),
                        create_date=create_date
                    ))
    cn_db.add_all(items_to_add)
    cn_db.commit()