from starlette.testclient import TestClient
import json

from app.main import app

client = TestClient(app)

def test_route_exist():
    response = client.get('/recommendation/?page=0&size=25')
    assert response.status_code is 200
    assert len(response.json()) is 25

def test_pagination_validation():
    response = client.get('/recommendation/')
    assert response.status_code is 200
    assert len(response.json()) is 25

def test_pagination_size():
    response = client.get('/recommendation/?page=0&size=5')
    assert response.status_code is 200
    assert len(response.json()) is 5

def test_popular_exist():
    response = client.get('/recommendation/popular/')
    assert response.status_code is 200
    assert len(response.json()) is not 0

def test_update_recs():
    response = client.put('/recommendation/big-red-button')
    assert response.status_code is 200
    assert response.json()['message'] == "Ok"

def test_send_stats():
    data = [{
        "userId": "c38f2d55-47e2-494a-a185-69225d727f5f",
        "itemId": "834c315b-9ef4-4908-96aa-c450e37cbd7e",
        "categoryId": "b6ec2b74-2bab-404b-a014-a99b126b3a50",
        "actionType": 1,
        "createDate": "2024-04-13T12:15:58.850Z"
    }]
    response = client.post('/recommendation/send-stats/', content=json.dumps(data))
    assert response.status_code is 200
    assert response.json() is None

def test_send_stats_empty():
    response = client.post('/recommendation/send-stats/', content=json.dumps([]))
    assert response.status_code is 200
    assert response.json() is None