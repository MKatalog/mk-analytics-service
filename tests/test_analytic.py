from starlette.testclient import TestClient

from app.main import app

client = TestClient(app)

def test_exist():
    response = client.get('/analytic/?id=92eee4b1-d9c7-44f8-8a9c-d8511144e47b')
    assert response.status_code is 200
    assert len(response.json()) is not 0

def test_validation():
    response = client.get('/analytic/?id=''')
    assert response.status_code is 200
    assert len(response.json()) is not 0