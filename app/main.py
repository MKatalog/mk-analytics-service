from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware

from .recomend import router as recomend
from .recomend import models as RecomendModels
from .analytic import models as AnalyticModels
from .analytic import router as analytic
from .db.cn import engine
from .service import load_html

RecomendModels.Base.metadata.create_all(bind=engine)
AnalyticModels.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(recomend.router)
app.include_router(analytic.router)

origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/')
async def root():
    return HTMLResponse(load_html(), status_code=200)