import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.neighbors import NearestNeighbors
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import hstack

from sqlalchemy import DDL
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from .models import Recommendations
from ..config import get_config

class DescriptionVectorizer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.vectorizer = TfidfVectorizer()

    def fit(self, X, y=None):
        self.vectorizer.fit(X)
        return self

    def transform(self, X):
        return self.vectorizer.transform(X)

class CategoryEncoder(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.encoder = OneHotEncoder()

    def fit(self, X, y=None):
        self.encoder.fit(X)
        return self

    def transform(self, X):
        return self.encoder.transform(X)

def _generate_recommendations(user_actions_df, items_df, item_features, nn_model, num_recommendations=3):
    recommendations = {}
    for user_id in user_actions_df['user_id'].unique():
        user_actions = user_actions_df[user_actions_df['user_id'] == user_id]
        
        user_preferences = user_actions.groupby('item_id')['action_score'].mean().reset_index()
        
        for _, row in user_preferences.iterrows():
            item_id = row['item_id']
            item_idx = items_df[items_df['item_id'] == item_id].index[0]
            similar_indices = nn_model.kneighbors(item_features[item_idx].reshape(1, -1), n_neighbors=num_recommendations, return_distance=False)
            similar_item_indices = similar_indices.flatten()[1:]
            similar_items = items_df.iloc[similar_item_indices]['item_id'].tolist()
            recommendations.setdefault(user_id, []).extend(similar_items)

    return recommendations

def _generate_item_pool(user_actions_df, items_df, item_features, nn_model, num_items_in_pool=10):
    num_items_in_pool = min(num_items_in_pool, item_features.shape[0])
    
    representative_recommendations = _generate_recommendations(user_actions_df, items_df, item_features, nn_model, num_items_in_pool)

    item_pool = set()
    for recommended_items in representative_recommendations.values():
        item_pool.update(recommended_items)
    
    return item_pool

def generate_general_recommendations(user_actions_df: pd.DataFrame, items_df: pd.DataFrame, db: Session) -> JSONResponse:
    try:
        config = get_config()

        unique_users_count = user_actions_df['user_id'].nunique()

        description_vectorizer = DescriptionVectorizer()
        description_features = description_vectorizer.fit_transform(items_df['description'])

        category_encoder = CategoryEncoder()
        category_features = category_encoder.fit_transform(items_df[['category_id']])

        item_features = hstack([description_features, category_features])

        nn_model = NearestNeighbors(metric='cosine')
        nn_model.fit(item_features)

        item_pool = _generate_item_pool(user_actions_df, items_df, item_features, nn_model, unique_users_count)

        item_popularity = user_actions_df.groupby('item_id')['action_score'].sum().reset_index()
        item_popularity = item_popularity.rename(columns={'action_score': 'popularity_score'})
        top_items_by_popularity = item_popularity.sort_values(by='popularity_score', ascending=False).head(20)

        items_to_insert: list[Recommendations] = []

        items_to_insert.extend(list(map(lambda x: Recommendations(id=x), list(item_pool)[:600])))
        items_to_insert.extend(list(map(lambda x: Recommendations(id=x, is_popular=True), top_items_by_popularity['item_id'].to_list())))

        db.execute(DDL(f"TRUNCATE {config.cn.cn_db_name}.recommendations"))
        db.add_all(items_to_insert)
        db.commit()

        return JSONResponse(content={"message": "Ok"}, status_code=200)
    except Exception as err:
        return JSONResponse(content={"message": str(err.args)}, status_code=503)


