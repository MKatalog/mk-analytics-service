import pandas as pd

from sqlalchemy.orm import Session, Query
from sqlalchemy.sql import func
from uuid import uuid4
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from . import models, schemas
from ..db.models import Products, ProductLinks, Categories

from .utils import generate_general_recommendations

def _race_score(row):
    if row['action_type'] == 1:
        return 2
    if row['action_type'] == 2:
        return 10
    
    return 0

def _products_query(psql_db: Session, filter_list) -> Query[any]:
        return psql_db.query(
        Products.Product, 
        func.count(ProductLinks.ProductLink.Id).label('ProductLinksCount'),
        func.min(ProductLinks.ProductLink.Price).label('PricesFrom'),
        func.max(ProductLinks.ProductLink.Price).label('PricesTo'),
        func.min(ProductLinks.ProductLink.Quantity).label('QuantitesFrom'),
        func.max(ProductLinks.ProductLink.Quantity).label('QuantitesTo'),
        func.now().label('DeliveryTimesFrom'),
        func.now().label('DeliveryTimesTo'),
        Categories.Category.Name.label("CategoryName")
        ).join(
            ProductLinks.ProductLink,
            Products.Product.Id == ProductLinks.ProductLink.ProductId
        ).join(
            Categories.Category,
            Categories.Category.Id == Products.Product.CategoryId
        ).group_by(
            Products.Product.Id,
            Categories.Category.Name
        ).filter(Products.Product.Id.in_(filter_list))

def _product_mapper(sql_data: list[any]):
    return list(
        map(lambda x: schemas.Product(
        id=x.Product.Id,
        productType=x.CategoryName,
        imagePath=x.Product.Image,
        name=x.Product.Name,
        productLinksCount=x.ProductLinksCount,
        prices=schemas.Between[float](**{
            "from": x.PricesFrom,
            "to": x.PricesTo
        }),
        quantites=schemas.Between[int](**{
            "from": x.QuantitesFrom,
            "to": x.QuantitesTo
        }),
        deliveryTimes=schemas.Between[str](**{
            "from": x.DeliveryTimesFrom.isoformat(),
            "to": x.DeliveryTimesTo.isoformat()
        })
    ), sql_data))

def insert_metrics(db: Session, recomends: list[schemas.MetricParam]):
    mapped_recomends = list(map(lambda x: models.Metrics(
        id=str(uuid4()), 
        user_id=x.user_id, 
        item_id=x.item_id,
        category_id=x.category_id,
        action_type=x.action_type,
        create_date=x.create_date),
        recomends))

    db.add_all(mapped_recomends)
    db.commit()

def update_global_recommendations(cn_db: Session, psql_db: Session) -> JSONResponse:
    products = pd.DataFrame(jsonable_encoder(psql_db.query(Products.Product).all()))
    products = products.rename(columns={
        'Id': 'item_id',
        'Name': 'item_name',
        'Image': 'image',
        'Description': 'description',
        'CategoryId': 'category_id'
    })

    users_metrics = pd.DataFrame(jsonable_encoder(cn_db.query(models.Metrics).all()))
    users_metrics['action_score'] = users_metrics.apply(_race_score, axis=1)

    return generate_general_recommendations(users_metrics, products, cn_db)

def get_popular_products(cn_db: Session, psql_db: Session) -> list[schemas.Product]:
    popular = list(map(lambda x: x.id, cn_db.query(models.Recommendations).where(models.Recommendations.is_popular == True)))
    query = _products_query(psql_db, popular)

    result = query.all()

    return _product_mapper(result)

def get_recommendations(params: schemas.RecommendationPagination, cn_db: Session, psql_db: Session) -> list[schemas.Product]:
    recommendations = list(map(lambda x: x.id, cn_db.query(models.Recommendations).where(models.Recommendations.is_popular == False)))
    query = _products_query(psql_db, recommendations)

    result = query.offset((params.page) * params.size).limit(params.size).all()

    return _product_mapper(result)