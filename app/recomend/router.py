from fastapi import APIRouter, Depends

from sqlalchemy.orm import Session
from ..db.cn import get_cn_db
from ..db.psql import get_psql_db
from .schemas import MetricParam, Product, RecommendationPagination
from . import service

router = APIRouter()

@router.post('/recommendation/send-stats', tags=['recommendation'])
def send_stats(params: list[MetricParam], db: Session = Depends(get_cn_db)):
    return service.insert_metrics(db, params)

@router.put('/recommendation/big-red-button', tags=['recommendation'])
def update_recommendations(cn_db: Session = Depends(get_cn_db), psql_db: Session = Depends(get_psql_db)):
    return service.update_global_recommendations(cn_db, psql_db)

@router.get('/recommendation/', tags=['recommendation'], response_model=list[Product])
def get_recommendations(page: int = 0, size: int = 25, cn_db: Session = Depends(get_cn_db), psql_db: Session = Depends(get_psql_db)):
    return service.get_recommendations(RecommendationPagination(page=page, size=size), cn_db, psql_db)

@router.get('/recommendation/popular', tags=['recommendation'], response_model=list[Product])
def get_popular( cn_db: Session = Depends(get_cn_db), psql_db: Session = Depends(get_psql_db)):
    return service.get_popular_products(cn_db, psql_db)