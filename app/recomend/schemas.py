from datetime import datetime
from pydantic import BaseModel, UUID4, AliasGenerator, ConfigDict, Field
from pydantic.alias_generators import to_camel, to_pascal, to_snake
from typing import Optional, TypeVar, Generic
from .models import ActionType

T = TypeVar("T", int, float, str)

class MetricParam(BaseModel):
    user_id: str
    item_id: str
    category_id: str
    action_type: ActionType
    create_date: datetime

    model_config = ConfigDict(
        alias_generator=AliasGenerator(
            validation_alias=lambda field_name: to_camel(field_name),
            serialization_alias=lambda field_name: to_snake(field_name),
        )
    )

class Between(BaseModel, Generic[T]):
    frm: T = Field(..., alias='from')
    to: T

class Product(BaseModel):
    id: UUID4
    name: str
    productType: str
    productLinksCount: int
    imagePath: Optional[str]
    prices: Between[float]
    deliveryTimes: Between[str]
    quantites: Between[int]

class RecommendationPagination(BaseModel):
    page: int = 0
    size: int = 25