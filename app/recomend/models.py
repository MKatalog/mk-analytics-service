from clickhouse_sqlalchemy import engines
from sqlalchemy import Column, DateTime, Enum, String, Boolean
from ..db.cn import Base
from ..config import get_config
import enum

_config = get_config()

class ActionType(enum.Enum):
    PRODUCT_CLICK = 1
    PRODUCT_LIKE = 2

class Metrics(Base):
    __tablename__='metrics'
    __table_args__=(
        engines.MergeTree(order_by=['id']),
        {'schema': _config.cn.cn_db_name}
    )

    id=Column(String, primary_key=True, nullable=False)
    user_id=Column(String, nullable=False)
    item_id=Column(String, nullable=False)
    category_id=Column(String, nullable=False)
    action_type=Column(Enum(ActionType), nullable=False)
    create_date=Column(DateTime, nullable=False)

class Recommendations(Base):
    __tablename__='recommendations'
    __table_args__=(
        engines.MergeTree(order_by=['id']),
        {'schema': _config.cn.cn_db_name}
    )

    id=Column(String, primary_key=True, nullable=False)
    is_popular=Column(Boolean, nullable=False, default=False)