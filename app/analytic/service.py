from sqlalchemy.orm import Session
from datetime import datetime

from .schemas import Statistic
from .models import HistoryStatistic, StatisticTypeEnum

def _get_title(type: StatisticTypeEnum) -> str:
    if type == StatisticTypeEnum.PRICE:
        return 'Изменение цены'
    if type == StatisticTypeEnum.QUANTITY:
        return 'Изменение количества товаров на складе'
    if type == StatisticTypeEnum.DELIVERY_TIME:
        return 'Изменение сроков поставок'

def _filter_by_type(value: HistoryStatistic, type: StatisticTypeEnum):
    if value.stat_type == type:
        return True
    else:
        return False

def _sort_by_key(x: list[str], y: list[str]) -> tuple[list[str], list[str]]:
    ziped = dict(zip(x, y))
    date_tuples = [(datetime.strptime(date, "%Y-%m-%dT%H:%M:%S"), value) for date, value in ziped.items()]
    sorted_date_tuples = sorted(date_tuples)
    sorted_date_dict = {date.strftime("%Y-%m-%dT%H:%M:%S"): value for date, value in sorted_date_tuples}

    return sorted_date_dict.keys(), sorted_date_dict.values()

def get_analytic_stats_by_product_id(id: str, cn_db: Session) -> list[Statistic]:
    query = cn_db.query(HistoryStatistic).where(HistoryStatistic.product_id == id).all()

    res = []

    for type in [StatisticTypeEnum.PRICE, StatisticTypeEnum.QUANTITY, StatisticTypeEnum.DELIVERY_TIME]:
        query_by_type = list(filter(lambda x: _filter_by_type(x, type), query))
        x, y = _sort_by_key(list(map(lambda x: x.x, query_by_type)), list(map(lambda x: x.y, query_by_type)))
        res.append(Statistic(
            type=type,
            title=_get_title(type),
            x=x,
            y=y
        ))

    return res