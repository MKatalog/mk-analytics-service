from pydantic import BaseModel, UUID4, AliasGenerator, ConfigDict, Field

from .models import StatisticTypeEnum


class Statistic(BaseModel):
    type: StatisticTypeEnum
    title: str
    x: list[str]
    y: list[str]