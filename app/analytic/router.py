from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from ..db.cn import get_cn_db
from . import service, schemas

router = APIRouter()

@router.get('/analytic/', tags=['analytic'], response_model=list[schemas.Statistic])
def get_analytic_stats(id: str, cn_db: Session = Depends(get_cn_db)):
    return service.get_analytic_stats_by_product_id(id, cn_db)