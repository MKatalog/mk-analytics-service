from clickhouse_sqlalchemy import engines
from sqlalchemy import Column, DateTime, Enum, String, Float
from ..db.cn import Base
from ..config import get_config
import enum

_config = get_config()

class StatisticTypeEnum(enum.Enum):
    PRICE = 1
    QUANTITY = 2
    DELIVERY_TIME = 3

class HistoryStatistic(Base):
    __tablename__='history-statistic'
    __table_args__=(
        engines.MergeTree(order_by=['id']),
        {'schema': _config.cn.cn_db_name}
    )

    id=Column(String, primary_key=True, nullable=False)
    product_id=Column(String, nullable=False)
    stat_type=Column(Enum(StatisticTypeEnum), nullable=False)
    x=Column(String, nullable=False)
    y=Column(String, nullable=False)
    create_date=Column(DateTime, nullable=False)