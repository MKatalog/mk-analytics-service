import os

def load_html() -> str:
    dir = os.path.abspath(os.curdir)
    html_file = open(os.path.join(dir, 'docs/index.html'), 'r', encoding='utf-8')

    return html_file.read()