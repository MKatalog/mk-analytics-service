from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

from ..config import get_config

load_dotenv()

_config = get_config()

engine = create_engine(_config.psql.psql_conn, echo=True)
SessionLocal = sessionmaker(bind=engine, autoflush=False)

def get_psql_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()