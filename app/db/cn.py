from sqlalchemy import create_engine, DDL, MetaData
from sqlalchemy.orm import sessionmaker
from clickhouse_sqlalchemy import get_declarative_base
from dotenv import load_dotenv

from ..config import get_config

load_dotenv()

_config = get_config()

engine = create_engine(_config.cn.cn_conn)
SessionLocal = sessionmaker(bind=engine, autoflush=False)
Session = SessionLocal()
Session.execute(DDL(f'CREATE DATABASE IF NOT EXISTS {_config.cn.cn_db_name}'))
Session.close()
Base = get_declarative_base(metadata=MetaData(schema=_config.cn.cn_db_name))

def get_cn_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()