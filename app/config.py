import os
from dotenv import load_dotenv
from pydantic_settings import BaseSettings
from functools import lru_cache

load_dotenv()

class PsqlConfig:
    psql_conn: str = os.environ['PSQL_DB_CONN']

class CnConfig:
    cn_conn: str = os.environ['CH_DB_CONN']
    cn_db_name: str = os.environ['CH_DB_NAME']
    cn_fake_count: int = int(os.environ['CN_FAKE_COUNT']) if os.environ['CN_FAKE_COUNT'] else 1000

class Config(BaseSettings):
    port: int = 5100
    cn: CnConfig = CnConfig()
    psql: PsqlConfig = PsqlConfig()

@lru_cache()
def get_config() -> Config:
    return Config()